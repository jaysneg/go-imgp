module gitlab.com/jaysneg/go-imgp

go 1.21

require (
	github.com/disintegration/imaging v1.6.2
	github.com/gofrs/uuid v4.4.0+incompatible
	github.com/stretchr/testify v1.8.4
	golang.org/x/sync v0.3.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
