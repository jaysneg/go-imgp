package imgp_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/jaysneg/go-imgp"
	"gitlab.com/jaysneg/go-imgp/datatype"
	"gitlab.com/jaysneg/go-imgp/filesystem"
)

func deleteFalder(path string) {
	err := os.RemoveAll(path)
	if err != nil {
		panic(err)
	}
}

func Test_Convert_FileSystem(t *testing.T) {
	testImgPath := "./test-img"
	itemsFolder := "files"
	file, err := os.Open("./test-fixtures/imgs/m_merged.jpg")
	require.NoError(t, err)

	defer file.Close()
	defer deleteFalder(testImgPath)

	require.NoError(t, err)

	sizes := []datatype.SubImageSizes{
		{
			Name:   "middle2",
			Width:  1000,
			Height: 600,
		},
		{
			Name:   "middle",
			Width:  350,
			Height: 300,
		},
		{
			Name:   "small",
			Width:  160,
			Height: 160,
		},
		{
			Name:   "micro",
			Width:  60,
			Height: 80,
		},
	}

	fs := filesystem.New(&filesystem.Config{
		RootDir:     testImgPath,
		ItemsFolder: itemsFolder,
		SubSizes:    sizes,
	})

	proc := imgp.New(&imgp.Config{
		DataStore: fs,
	})

	res, err := proc.Convert("test.jpg", file)
	require.NoError(t, err)

	files, err := os.ReadDir(res.Folder)
	require.NoError(t, err)

	var count int
	for range files {
		count++
	}

	assert.Equal(t, 5, count)

	_, err = os.Stat(testImgPath)
	assert.Falsef(t, os.IsNotExist(err), "no path %s", testImgPath)

	_, err = os.Stat(testImgPath + "/" + itemsFolder)
	assert.Falsef(t, os.IsNotExist(err), "no path %s", testImgPath+"/"+itemsFolder)

	_, err = os.Stat(testImgPath + "/" + res.Path)
	assert.Falsef(t, os.IsNotExist(err), "no main file %s", testImgPath+"/"+res.Path)

	for _, size := range sizes {
		filePath := res.Folder + "/" + size.Name + "-" + res.Name
		_, err = os.Stat(filePath)
		assert.Falsef(t, os.IsNotExist(err), "no main file %s", filePath)
	}
}
