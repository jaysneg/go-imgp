package datatype

import "io"

type File interface {
	Open() (io.Reader, error)
	Close() error
}

type FileConfig struct {
	File             io.Reader
	PublicPath       string
	OriginalFilePath string
	NewFileName      string
}

type Image struct {
	Folder    string
	URLFolder string
	Path      string
	Name      string
	DataStore DataStore
}

type SubImageSizes struct {
	Name   string
	Width  int
	Height int
}

type DataStore interface {
	DataStoreName() string
	Process(*FileConfig) error
	RootDirPath() string
	ItemsFolderPath() string
}
