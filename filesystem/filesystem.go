package filesystem

import (
	"bytes"
	"fmt"
	"io"
	"os"

	"github.com/disintegration/imaging"
	"gitlab.com/jaysneg/go-imgp/datatype"
	"gitlab.com/jaysneg/go-imgp/util"
	"golang.org/x/sync/errgroup"
)

type FileSystem struct {
	subSizes    []datatype.SubImageSizes
	name        string
	rootDir     string
	itemsFolder string
}

type Config struct {
	SubSizes    []datatype.SubImageSizes
	RootDir     string
	ItemsFolder string
}

func (fs *FileSystem) DataStoreName() string {
	return fs.name
}

func (fs *FileSystem) RootDirPath() string {
	return fs.rootDir
}

func (fs *FileSystem) ItemsFolderPath() string {
	return fs.itemsFolder
}

func New(c *Config) *FileSystem {
	return &FileSystem{
		subSizes:    c.SubSizes,
		rootDir:     c.RootDir,
		itemsFolder: c.ItemsFolder,
		name:        "filesystem",
	}
}

func (fs *FileSystem) Process(fileConfig *datatype.FileConfig) error {
	fBytesChan := make(chan []byte, 1)

	subSizes := fs.subSizes

	initErrGroup := new(errgroup.Group)

	processErrGroup := new(errgroup.Group)

	initErrGroup.Go(func() error {
		if _, err := os.Stat(fileConfig.PublicPath); os.IsNotExist(err) {
			err = os.MkdirAll(fileConfig.PublicPath, 0755)
			if err != nil {
				return err
			}
		}

		return nil
	})

	initErrGroup.Go(func() error {
		defer close(fBytesChan)

		fBytes, err := io.ReadAll(fileConfig.File)
		if err != nil {
			return err
		}

		fBytesChan <- fBytes

		return nil
	})

	initErrGroup.Wait()

	// Processing
	fBytes := <-fBytesChan

	processErrGroup.Go(func() error {
		err := fs.createOriginal(fBytes, fileConfig.PublicPath, fileConfig.OriginalFilePath)
		if err != nil {
			return err
		}

		return nil
	})

	processErrGroup.Go(func() error {
		err := fs.createSubImages(fBytes, subSizes, fileConfig.PublicPath, fileConfig.NewFileName)
		if err != nil {
			return err
		}

		return nil
	})

	return processErrGroup.Wait()
}

func (fs *FileSystem) createSubImages(file []byte, subSizes []datatype.SubImageSizes, publicPath, newFileName string) error {
	errGroup := new(errgroup.Group)

	for _, val := range subSizes {
		kind := val.Name
		width := val.Width
		height := val.Height

		errGroup.Go(func() error {
			i, err := imaging.Decode(bytes.NewReader(file))
			if err != nil {
				return err
			}

			return imaging.Save(util.Resize(i, width, height), fmt.Sprint(publicPath, "/", kind, "-", newFileName))
		})
	}

	return errGroup.Wait()
}

func (*FileSystem) createOriginal(file []byte, publicPath, originalFilePath string) error {
	err := os.WriteFile(originalFilePath, file, 0666)
	if err != nil {
		return err
	}

	return nil
}
