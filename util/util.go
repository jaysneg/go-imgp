package util

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"image"
	"math"

	"github.com/disintegration/imaging"
	"github.com/gofrs/uuid"
)

func GetUUID() (string, error) {
	u2, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	return u2.String(), err
}

func Resize(img image.Image, maxWidth, maxHeight int) image.Image {
	srcW := img.Bounds().Dx()
	srcH := img.Bounds().Dy()

	aspectW := float64(srcW) / float64(maxWidth)
	aspectH := float64(srcH) / float64(maxHeight)
	resWidth := float64(maxWidth)
	resHeight := float64(maxHeight)

	if aspectW > 1.0 || aspectH > 1.0 {
		if aspectW > aspectH {
			resHeight = float64(srcH) / float64(aspectW)
		} else {
			resWidth = float64(srcW) / float64(aspectH)
		}

		img = imaging.Resize(img, int(math.Max(1.0, math.Floor(resWidth+0.5))), int(math.Max(1.0, math.Floor(resHeight+0.5))), imaging.Lanczos)
	}

	return img
}

func NewFileName(ext string) string {
	b := make([]byte, 4)
	rand.Read(b)
	newName := hex.EncodeToString(b)

	return fmt.Sprint(newName, ext)
}
