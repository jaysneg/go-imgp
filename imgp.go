package imgp

import (
	"fmt"
	"io"
	"path/filepath"

	_ "image/gif"
	_ "image/png"

	"gitlab.com/jaysneg/go-imgp/datatype"
	"gitlab.com/jaysneg/go-imgp/util"
)

type Processor struct {
	dataStore datatype.DataStore
}

type Config struct {
	DataStore datatype.DataStore
}

func New(c *Config) *Processor {
	return &Processor{
		dataStore: c.DataStore,
	}
}

func (p *Processor) Convert(fileName string, file io.Reader) (*datatype.Image, error) {
	uniqueFolderName, err := util.GetUUID()
	if err != nil {
		return nil, err
	}

	rootPath := p.dataStore.RootDirPath()
	newFileName := util.NewFileName(filepath.Ext(fileName))
	dirPath := fmt.Sprint(p.dataStore.ItemsFolderPath(), "/", uniqueFolderName)
	publicPath := fmt.Sprint(rootPath, "/", dirPath)
	imgPath := fmt.Sprint(dirPath, "/", newFileName)
	originalFilePath := fmt.Sprint(publicPath, "/", newFileName)

	res := &datatype.Image{
		Folder:    publicPath,
		URLFolder: dirPath,
		Path:      imgPath,
		Name:      newFileName,
		DataStore: p.dataStore,
	}

	err = p.dataStore.Process(&datatype.FileConfig{
		OriginalFilePath: originalFilePath,
		File:             file,
		NewFileName:      newFileName,
		PublicPath:       publicPath,
	})
	if err != nil {
		return res, err
	}

	return res, nil
}
